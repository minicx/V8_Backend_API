export interface ILinkerCreationParams {
  readonly title: string;
  readonly parent?: string;
}
export interface IRawLinker extends ILinkerCreationParams {
  readonly createdAt: string;
  readonly updateAt: string;
}
export interface ILinker extends ILinkerCreationParams {
  readonly createdAt: Date;
  readonly updateAt: Date;
}
