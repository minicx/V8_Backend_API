export interface IFormCreationParams {
  readonly title: string;
  readonly description: string;
}
export interface IRawForm extends IFormCreationParams {
  readonly id: number;
  readonly createdAt: string;
  readonly updateAt: string;
}
export interface IForm extends IFormCreationParams {
  readonly id: number;
  readonly createdAt: Date;
  readonly updateAt: Date;
}
