// Type definitions for BackendApi src/api.js 0.05
// Project: BackendApi
// Definitions by: minicx <[https://git.disroot.org/minicx]>
import { IForm, IFormCreationParams } from './interfaces/form.interface';
import { ILinker, ILinkerCreationParams } from './interfaces/linker.interface';
declare class Api {
  protected readonly api: {
    main: string;
    forms: string;
    linkers: string;
    auth: string;
  };

  constructor(EndpointLink: string);
  protected handleError(err: Error);
  login(input: {
    username: string;
    password: string;
  }): Promise<string | undefined>;
  getForms(): Promise<IForm[]>;
  createForm(form: IFormCreationParams, token: string): Promise<IForm>;
  getLinkers(): Promise<ILinker[]>;
  createLinker(linker: ILinkerCreationParams, token: string): Promise<ILinker>;
}

export default Api;
