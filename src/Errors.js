class WrongDataError extends Error {
  constructor() {
    super('You passed ununique data!');
  }
}

class UnauthorizedError extends Error {
  constructor() {
    super('You passed wrong token!');
  }
}
class InternalError extends Error {
  constructor() {
    super('Server has internal error!');
  }
}
class UnknownError extends Error {
  constructor(statusCode) {
    this.statusCode = statusCode;
    super(`Unknown error happend with ${this.statusCode} code`);
  }
}
export default {
  UnauthorizedError,
  UnknownError,
  InternalError,
  WrongDataError,
};
