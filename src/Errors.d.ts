// Type definitions for BackendApi src/Errors.js 0.05
// Project: BackendApi
// Definitions by: minicx <[https://git.disroot.org/minicx]>
export namespace Errors {
  class WrongDataError extends Error {
    constructor();
  }
  class UnauthorizedError extends Error {
    constructor();
  }
  class InternalError extends Error {
    constructor();
  }
  class UnknownError extends Error {
    constructor(statusCode: number);
  }
}
