import Errors from './Errors.js';

const headers = {
  'Content-Type': 'application/json',
};

class Api {
  api = {
    main: '',
    forms: '',
    linker: '',
    auth: '',
  };
  constructor(EndpointLink) {
    this.api.main = EndpointLink;
    this.api.forms = EndpointLink + '/api/forms';
    this.api.linker = EndpointLink + '/api/linkers';
    this.api.auth = EndpointLink + '/auth';
  }
  handleError(err) {
    switch (err.statusCode) {
      case 401:
        if (err.message === 'Unauthorized') {
          throw new Errors.UnauthorizedError();
        } else {
          throw new Errors.UnknownError(401);
        }
      case 500:
        if (err.message === 'Internal server error') {
          throw new Errors.InternalError();
        } else {
          throw new Errors.UnknownError(500);
        }
      case 400:
        if (err.reason === 'This not a correct data') {
          throw new Errors.WrongDataError();
        } else {
          throw new Errors.UnknownError(400);
        }
    }
  }
  async login(input) {
    const result = await fetch(this.api.auth + '/login', {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(input),
    });
    if (result.ok) {
      return (await result.json()).access_token;
    } else {
      return undefined;
    }
  }
  async getForms() {
    const result = await fetch(this.api.forms);
    if (result.ok) {
      return (await result.json()).map((form) => {
        return {
          ...form,
          createdAt: new Date(form.createdAt),
          updatedAt: new Date(form.updatedAt),
        };
      });
    } else {
      return [];
    }
  }
  async createForm(form, token) {
    const headersWithAuth = Object.assign(
      { Authorization: 'Bearer ' + token },
      headers,
    );
    const result = await fetch(this.api.forms, {
      method: 'POST',
      headers: headersWithAuth,
      body: JSON.stringify(form),
    });
    if (result.ok) {
      const rawForm = await result.json();
      return {
        ...rawForm,
        createdAt: new Date(rawForm.createdAt),
        updatedAt: new Date(rawForm.updatedAt),
      };
    } else {
      this.handleError({ ...(await result.json()), statusCode: result.status });
    }
  }

  async getLinkers() {
    const result = await fetch(this.api.linker);
    if (result.ok) {
      return (await result.json()).map((linker) => {
        return {
          ...linker,
          createdAt: new Date(linker.createdAt),
          updatedAt: new Date(linker.updatedAt),
        };
      });
    } else {
      return [];
    }
  }
  async createLinker(linker, token) {
    const headersWithAuth = Object.assign(
      { Authorization: 'Bearer ' + token },
      headers,
    );
    const result = await fetch(this.api.linker, {
      method: 'POST',
      headers: headersWithAuth,
      body: JSON.stringify(linker),
    });
    if (result.ok) {
      const rawLinker = await result.json();
      return {
        ...rawLinker,
        createdAt: new Date(rawLinker.createdAt),
        updatedAt: new Date(rawLinker.updatedAt),
      };
    } else {
      this.handleError({ ...(await result.json()), statusCode: result.status });
    }
  }
}

export default Api;
